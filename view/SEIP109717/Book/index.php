<?php
session_start();
include_once '../../../vendor/autoload.php';
use \App\SEIP109717\Book\Book;
use App\SEIP109717\Message\Message;

$object= new Book();
$object=$object->index();
//print_r($object);
//die();
?>
<!DOCTYPE html>
<html lang="en">
  
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project</title>
    <link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  </head>
  <body>
  
  <div id="wrapper">
  <div class="list-group">
  
  <a href="" class="list-group-item  active text-center"><!--active hocce color fixed kore rakhbe aarea ke-->
  <!--list-group-item-danger diyeo aro kaj kora jabe-->
  
  <h1 class="list-group-item-heading glyphicon glyphicon-user">ATOMIC PROJECT</h1>
  <p class="list-group-item-text">This is Atomic project</p></a>
  
    </div><!-- #header -->
    
    </div>


  <div class="panel panel-default panel-danger">
   
  <div class="panel-heading ">

  <h3 class="panel-title text-center">
  <a href="http://localhost/crud/index.php">
  <span class="badge glyphicon glyphicon-home">
  Home</span></a>
  </h3>
  </div>
  
  <div id="jumbotron" class="jumbotron">
      <h4 class="text-center text-success">Book Title And Author List</h4>
      <div class="warning text-center text-success">  
      <?php
      
      if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])){
          
          echo message::flash();
        } 
       

      ?>
      </div>
   </div>
  <div class="jumbotron">
    
      
      <a href="create.php" class="btn btn-primary">Add New Book</a>

              <select class="btn btn-info">
                  <option>10</option>
                  <option>20</option>
                  <option>30</option>
                   <option>40</option>
                    <option>50</option>
              </select>
              <span id="" class="text-danger text-center">DOWNLOAD 
              <a class="btn btn-warning text-center" href="pdf.php">PDF</a>
              <a class="btn btn-success " href="xl.php">XL</a>
              <a class="btn btn-danger"style="opacity:.5" href="mail.php?id=<?php echo $obj['id'];?>">Email Send</a>
              <a class="btn btn-success"style="opacity:.5" href="trashed.php">Trashed</a>
          </span>
      <!--search item-->
      <form class="navbar-form navbar-right"  action="search.php" method="post"role="search">
        <div class="form-group">
          <input type="text" name="name" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-search" ></span></button>
      </form>
      
       <!--search item-->
      <form action="" method="">
            <table class="table table-bordered text-info" border="1">
            
                  <tr>
                  <th>SERIAL&dArr;</th>
                <th>NAME&dArr;</th>
              <th>BIRTHDAY &dArr;</th> 
              <th>ACTION&dArr;</th>
              </tr>
              <?php
               $slno=0;
              foreach($object as $obj):
                  $slno++;                      
              ?>
              
              <tr>

              <td><?php echo $slno; ?></td>
               <td><?php echo $obj['book_name'];?></td>
              <td><?php echo $obj['author']; ?></td> 
              <td>
            <button class="btn btn-warning"type="button" name="button" value="">
            <a href="view.php?id=<?php echo $obj['id']; ?>">View</a>
            </button>


            <button class="btn btn-info"type="button" name="button" value="">
            <a href="edit.php?id=<?php echo $obj['id'];?>">Edit</a>
            </button>


             <button class="btn btn-danger delete"type="button" name="button" value="">
             <a href="delete.php?id=<?php echo $obj['id'];?>">Delete</a>
             </button>
             <button  style="opacity:.8"class="btn btn-danger  trash"type="button" name="button" value="">
             <a href="trash.php?id=<?php echo $obj['id'];?>">Trash</a>
             </button>
             


              </td>
              </tr>
            
           <?php
                endforeach;
                                                
            ?>
             
            </table>
          </form>
      
                              <ul class="pagination">
              <li>
                <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li>
                <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
              </ul>
  </div>
  
<div id="footer" class="page-header text-center text-primary">
      <p>
        &copy; kamran Hossain. SEID-109717. PHP Batch-13
      </p>
    </div><!-- #footer -->
 
   </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../../js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/JavaScript"></script>
    <script>
    $(document).ready(function(){
      $('.delete').bind('click',function(e){
        
       var ok= confirm("Are You sure want o delete this data?");
       //consol.log(ok);
       //alert(typeof ok);
       if(!ok){
          e.preventDefault();
       }

      });
    });
    </script>
  
  </body>
  
</html>